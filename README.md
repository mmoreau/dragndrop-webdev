# DragNDrop-webdev

An example of using a drag & drop, when you drop images, they display below. 

## Functionalitie

* Displays the image when depositing in the block
* Delete an image when clicking

## JSFiddle 

https://jsfiddle.net/mmoreau/ya5tweq4/1/

## Information

In the "**script.js**" file you will find **two ways** to **upload files** (here it will be **images**) in **Drag & Drop**.

Do not store your data as BLOBs in databases, BLOB data types are large their queries. Store your images, documents rather as URLs or Path on the disk.

## ToDo

* [ ] Improve image sharpness
* [ ] Improve the size of the images for which proportional