(() => {

    /* .:: Retrieves HTML tag IDs to interact with them during actions, events. ::. */
    let dropBlock = document.getElementById("block");
    let dropBlockMessage = document.getElementById("block_message");
    let fileForm = document.getElementById("file");
    let container = document.getElementById("container");

    /* .:: Allow only extensions that are in the table ::. */
    let extension = ["png", "jpg", "gif", "jpeg", "webp"];

    /* .:: Upload files here more precisely images during selection by clicking on the button ::.  */
    fileForm.addEventListener("change", (e) => uploadPicture(e), false);

    /* .:: Triggers events when the image flies over the output of the upload area.  ::. */
    dropBlock.addEventListener("dragover", (e) => e.preventDefault(), false);

    /* .:: Upload the image when the image is dropped in the upload area. ::. */
    dropBlock.addEventListener("drop", (e) => uploadPicture(e), false);

    /* .:: Delete an image when you double click on it ::. */
    container.addEventListener("dblclick", (ev) => {container.removeChild(ev.target)}, false);


    /* .:: Function that allows you to upload an image to the screen when an event occurs ::. */
    const uploadPicture = (e) => {

        let file_obj;
        let file_length_allowed = 0;

        switch (e.type)
        {
            case "change":
                file_obj = e.target.files;
                break;

            case "drop":
                e.stopPropagation();
                e.preventDefault();

                file_obj = e.dataTransfer.files;
                break;
        }

        for (file of file_obj)
        {
            file_type = file.type.split("/");

            if (file_type[0] == "image") {
                if (extension.indexOf(file_type[1]) != -1) {

                    ++file_length_allowed;
                    createImage(file);
                }
            }
        }

        dropBlockMessage.textContent = file_obj.length + " File(s) # " + file_length_allowed + " Allowed";
    }


    /* .:: A function that allows you to create the image and display it on the page. ::. */
    const createImage = (object_file) => {

        /* .:: Another way to upload by displaying images in a container ::.

        let newImg = document.createElement("img");
        //newImg.classList.add("obj");
        newImg.file = file;
        container.appendChild(newImg);

        let reader = new FileReader();

        reader.onload = (function(evt){
            return function(ev) {
                evt.src = ev.target.result;
            };
        })(newImg);
        */


        /* .:: Way 2 ::. */

        let reader = new FileReader();

        reader.onload = (ev) => {
            let newImg = document.createElement("img");
            newImg.src = ev.target.result;
            container.appendChild(newImg);
        }

        reader.readAsDataURL(object_file);
    }

})();